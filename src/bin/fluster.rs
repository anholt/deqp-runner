use anyhow::{Context, Result};
use deqp_runner::fluster_command::{FlusterCommand, FlusterRunConfig};
use deqp_runner::{
    parallel_test, process_results, CommandLineRunOptions, Expectations, TestConfiguration,
};
use std::path::PathBuf;
use structopt::StructOpt;
use tempfile::tempdir;

use deqp_runner::mock_fluster::{mock_fluster, MockFluster};

#[derive(Debug, StructOpt)]
#[structopt(
    author = "Detlev Casanova <detlev.casanova@collabora.com>",
    about = "Wrap fluster in a runner to group and parallelize tests"
)]
struct Opts {
    #[structopt(subcommand)]
    subcmd: SubCommand,
}

#[derive(Debug, StructOpt)]
#[allow(clippy::large_enum_variant)]
enum SubCommand {
    #[structopt(name = "run")]
    Run(Run),
    #[structopt(name = "mock-fluster")]
    MockFluster(MockFluster),
}

#[derive(Debug, StructOpt)]
pub struct Run {
    #[structopt(long, help = "path to fluster binary")]
    fluster: PathBuf,

    #[structopt(flatten)]
    common: CommandLineRunOptions,

    #[structopt(
        long,
        default_value = "500",
        help = "Starting number of tests to include in each bin invocation (mitigates startup overhead)"
    )]
    tests_per_group: usize,

    #[structopt(
        long,
        default_value = "0",
        help = "Minimum number of tests to scale down to in each bin invocation (defaults to 0 to match tests_per_group)"
    )]
    min_tests_per_group: usize,

    #[structopt(long, help = "decoder to test")]
    decoders: Vec<String>,

    #[structopt(long, help = "test suite dir")]
    test_suites_dir: Option<PathBuf>,

    #[structopt(help = "arguments to fluster binary")]
    fluster_args: Vec<String>,
}

fn main() -> Result<()> {
    let opts = Opts::from_args();

    match opts.subcmd {
        SubCommand::Run(run) => {
            if run.decoders.is_empty() {
                anyhow::bail!("At least 1 decoder must be selected with --decoders");
            }

            run.common.setup()?;

            let fluster = FlusterCommand {
                run_config: FlusterRunConfig {
                    bin: run.fluster,
                    decoders: run.decoders,
                    test_suites_dir: run.test_suites_dir,
                },
                config: TestConfiguration::from_cli(&run.common)?,
                args: run.fluster_args,
                results_dir: tempdir().context("Creating tempdir")?.into_path(),
            };

            let mut tests = fluster.list_tests()?;

            let groups = fluster.group_tests_per_decoder(
                &mut tests,
                run.tests_per_group,
                run.min_tests_per_group,
                &run.common.sub_config,
                &[run
                    .common
                    .includes_regex()
                    .context("Parsing include regex")?],
            )?;

            let results = parallel_test(std::io::stdout(), groups)?;
            process_results(
                &results,
                &run.common.output_dir,
                run.common.summary_limit,
                Expectations::from_sub_run_config(&run.common.sub_config, "fluster"),
            )?;
        }

        SubCommand::MockFluster(mock) => {
            stderrlog::new().module(module_path!()).init().unwrap();
            mock_fluster(&mock)?;
        }
    }

    Ok(())
}
