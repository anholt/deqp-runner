use crate::mock_fluster::SubCommand::Run;
use anyhow::{bail, Context, Result};
use rand::Rng;
use std::fmt::Display;
use std::io::prelude::*;
use std::path::PathBuf;
use structopt::StructOpt;

/// Mock fluster that uses conventions in the test name to control behavior of the
/// test.  We use this for integration testing of fluster-runner.

#[derive(Debug, StructOpt)]
struct RunArgs {
    #[structopt(short = "j", long)]
    #[allow(dead_code)]
    jobs: usize,

    #[structopt(short = "d", long)]
    #[allow(dead_code)]
    decoder: String,

    #[structopt(long)]
    testvectors: Vec<String>,
}

#[derive(Debug, StructOpt)]
#[allow(clippy::large_enum_variant)]
enum SubCommand {
    #[structopt(name = "run")]
    Run(RunArgs),
    #[structopt(name = "list")]
    List,
}

#[derive(Debug, StructOpt)]
pub struct MockFluster {
    #[structopt(long)]
    output: Option<PathBuf>,

    #[structopt(long)]
    #[allow(dead_code)]
    test_suites_dir: Option<PathBuf>,

    #[structopt(subcommand)]
    subcmd: SubCommand,
}

enum FlusterResult {
    Pass,
    Skip,
    Fail,
    Error,
}

impl Display for FlusterResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                FlusterResult::Pass => "Success",
                FlusterResult::Skip => "Not Run",
                FlusterResult::Fail => "Fail",
                FlusterResult::Error => "Error",
            }
        )
    }
}

pub fn list_decoders() {
    println!(
        "
List of available decoders:
CODEC
    my-decoder-SW: Test decoder 1
    my-decoder-HW: Test decoder 2
    my-decoder-TEST: Test decoder 3
"
    )
}

fn fluster_result(decoder: &str, test: &str, result: FlusterResult) {
    println!("[Test_Suite  ] ({decoder}) {test}     ... {result}");
}

pub fn mock_fluster(mock: &MockFluster) -> Result<()> {
    match &mock.subcmd {
        Run(r) => {
            let (tests, decoder) = (&r.testvectors, &r.decoder);
            std::fs::create_dir_all(mock.output.as_ref().unwrap())
                .context("Creating mock fluster output")?;

            for test in tests {
                if test.ends_with("+pass") {
                    fluster_result(decoder, test, FlusterResult::Pass);
                } else if test.ends_with("+fail") {
                    fluster_result(decoder, test, FlusterResult::Fail);
                } else if test.ends_with("+skip") {
                    fluster_result(decoder, test, FlusterResult::Skip);
                } else if test.ends_with("+error") {
                    fluster_result(decoder, test, FlusterResult::Error);
                } else if test.ends_with("+flake") {
                    if rand::thread_rng().gen::<bool>() {
                        fluster_result(decoder, test, FlusterResult::Pass);
                    } else {
                        fluster_result(decoder, test, FlusterResult::Fail);
                    }
                } else if test.ends_with("+crash") {
                    panic!("crashing!")
                } else if test.ends_with("+late_crash") {
                    fluster_result(decoder, test, FlusterResult::Pass);
                    std::io::stdout().flush().unwrap();
                    panic!("crashing!")
                } else if test.ends_with("+timeout") {
                    std::io::stdout().flush().unwrap();
                    // Simulate a testcase that doesn't return in time by infinite
                    // looping.
                    #[allow(clippy::empty_loop)]
                    loop {}
                } else {
                    bail!("Invalid test")
                }
            }
        }
        SubCommand::List => {
            list_decoders();
            return Ok(());
        }
    };

    Ok(())
}
