use ::deqp_runner::{Expectations, RunnerResults};
use anyhow::{Context, Result};
use std::collections::HashMap;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::process::Output;

/// Integration test binary.  See https://matklad.github.io/2021/02/27/delete-cargo-integration-tests.html
mod deqp_runner;
mod fluster_runner;
mod gtest_runner;
mod piglit_runner;

// All the output we capture from an invocation of deqp-runner
struct RunnerCommandResult {
    args_map: HashMap<String, Vec<String>>,
    status: std::process::ExitStatus,
    stdout: String,
    stderr: String,
    output_dir_contents: Vec<String>,

    results: Result<RunnerResults>,
    expectations: Result<Expectations>,
}

impl RunnerCommandResult {
    fn from_output(
        command: &std::process::Command,
        output: &Output,
        temp_dirs: Vec<tempfile::TempDir>,
    ) -> Result<RunnerCommandResult> {
        let args = command
            .get_args()
            .map(|arg| arg.to_string_lossy().into_owned())
            .collect::<Vec<String>>();
        let args_map = from_args_to_map(args);

        let output_dir_path: &Path = Path::new(
            args_map
                .get("output")
                .ok_or(anyhow::Error::msg("output arg not found"))?[0]
                .as_str(),
        );
        let output_dir_contents = dir_contents(output_dir_path);

        let results_path = output_dir_path.to_owned().join("results.csv");
        let results = std::fs::File::open(&results_path)
            .with_context(|| format!("opening {:?}", &results_path))
            .and_then(|mut f| RunnerResults::from_csv(&mut f).context("reading results.csv"));

        let expectations_json_path = output_dir_path.to_owned().join("expectations.json");
        let expectations = std::fs::File::open(&expectations_json_path)
            .with_context(|| format!("opening {:?}", &expectations_json_path))
            .and_then(|mut f| Expectations::from_json(&mut f).context("reading expectations.json"));

        // Debug knob, flip it to save the output dirs so you can look into why things failed.
        if false {
            for temp_dir in temp_dirs {
                let _ = temp_dir.into_path();
            }
        } else {
            for temp_dir in temp_dirs {
                let _ = temp_dir.close();
            }
        }

        Ok(RunnerCommandResult {
            args_map,
            status: output.status,
            stdout: String::from_utf8(output.stdout.clone()).context("UTF-8 of stdout")?,
            stderr: String::from_utf8(output.stderr.clone()).context("UTF-8 of stderr")?,
            output_dir_contents,
            results,
            expectations,
        })
    }
}

fn from_args_to_map(args: Vec<String>) -> HashMap<String, Vec<String>> {
    // Extract arguments from Command
    let mut args_map = HashMap::new();
    let mut iter = args.iter();
    while let Some(arg) = iter.next() {
        if arg.starts_with("--") {
            let key = arg.trim_start_matches("--").to_string();
            if let Some(value) = iter.next() {
                args_map
                    .entry(key)
                    .or_insert_with(Vec::new)
                    .push(value.clone());
            }
        }
    }

    args_map
}

pub fn tempfile<S: AsRef<str>>(data: S) -> Result<tempfile::TempPath> {
    let mut file = tempfile::NamedTempFile::new().context("creating tempfile")?;
    file.write(data.as_ref().as_bytes())
        .context("writing tempfile")?;
    Ok(file.into_temp_path())
}

pub fn lines_tempfile<S: AsRef<str>, I: IntoIterator<Item = S>>(
    lines: I,
) -> Result<tempfile::TempPath> {
    let mut file = tempfile::NamedTempFile::new().context("creating tempfile")?;
    for line in lines {
        writeln!(file, "{}", line.as_ref()).context("writing tempfile")?;
    }
    Ok(file.into_temp_path())
}

pub fn dir_contents(dir: &Path) -> Vec<String> {
    if let Ok(contents) = dir.read_dir() {
        contents
            .filter_map(|x| x.ok())
            .map(|x| x.file_name().to_string_lossy().as_ref().to_string())
            .collect()
    } else {
        Vec::new()
    }
}

// Returns the path to a test resource checked into git.
pub fn test_resource_path(filename: &str) -> PathBuf {
    let manifest = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    manifest.join("resources/test").join(filename)
}
