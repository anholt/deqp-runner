use super::*;
use ::deqp_runner::fluster_command::FlusterVector;
use ::deqp_runner::fluster_command::FlusterVectors;
use anyhow::{Context, Result};
use std::process::{Command, Stdio};

/// Builder for a mocked fluster-runner invocation
#[derive(Default)]
struct FlusterMock {
    baselines: Vec<tempfile::TempPath>,
    decoders: Vec<String>,
}

pub fn temp_fluster(tests: &Vec<String>) -> Result<tempfile::TempDir> {
    let temp_dir = tempfile::tempdir().context("Creating output dir")?;

    let codec = "codec";
    let tests_dir = temp_dir.path().join("test_suites").join(codec);
    std::fs::create_dir_all(&tests_dir).context("creating tests")?;

    let vectors = FlusterVectors {
        test_vectors: tests
            .iter()
            .map(|t| FlusterVector {
                name: t.to_string(),
            })
            .collect(),
    };

    let file = std::fs::File::create(tests_dir.join("suite.xml"))
        .context("Creating fluster suite file")?;
    serde_yaml::to_writer(file, &vectors).context("Writing tests to suite file")?;

    Ok(temp_dir)
}

impl FlusterMock {
    pub fn new() -> FlusterMock {
        Default::default()
    }

    pub fn run(&self, tests: Vec<String>) -> Result<RunnerCommandResult> {
        let output_dir = tempfile::tempdir().context("Creating output dir")?;

        // Get the location of our fluster-runner binary from rustc
        let fluster_runner = env!("CARGO_BIN_EXE_fluster-runner");

        let fluster_dir =
            temp_fluster(&tests).context("creating temporary fluster test_vectors")?;

        let mut cmd = Command::new(fluster_runner);
        let child = cmd.stdout(Stdio::piped()).stderr(Stdio::piped());
        let child = child.arg("run");

        let child = child.arg("--fluster");
        let child = child.arg(fluster_runner);

        let child = child.arg("--output");
        let child = child.arg(output_dir.path());

        let child = child.arg("--decoders");
        let child = child.args(&self.decoders);

        let child = child.arg("--test-suites-dir");
        let child = child.arg(fluster_dir.path());

        let child = child.arg("--timeout");
        let child = child.arg("1");

        for baseline_file in &self.baselines {
            child.arg("--baseline");
            child.arg(baseline_file);
        }

        child.arg("-j");
        child.arg("3");

        child.arg("--");
        child.arg("mock-fluster");

        let output = child
            .spawn()
            .with_context(|| format!("Spawning {:?}", fluster_runner))?
            .wait_with_output()
            .context("waiting for deqp-runner")?;

        Ok(RunnerCommandResult::from_output(
            &child,
            &output,
            vec![output_dir],
        )?)
    }

    pub fn with_baseline<S: AsRef<str>>(&mut self, data: S) -> &mut FlusterMock {
        self.baselines
            .push(tempfile(data).context("writing baselines").unwrap());
        self
    }

    pub fn with_decoders(&mut self, decoder: &[&str]) -> &mut FlusterMock {
        self.decoders
            .append(&mut decoder.iter().map(|d| d.to_string()).collect());
        self
    }

    pub fn with_decoder(&mut self, decoder: &str) -> &mut FlusterMock {
        self.decoders.push(decoder.to_string());
        self
    }
}

#[test]
fn basic_cases() {
    let mut baseline = String::new();
    let mut tests = vec![];
    let decoder = "my-decoder-SW";
    for i in 0..12 {
        baseline.push_str(&format!("{decoder}@fluster{i}+fail,Fail\n"));
        tests.push(format!("fluster{i}+fail"));
    }
    for i in 0..10 {
        tests.push(format!("fluster{i}+pass"));
    }
    for i in 0..11 {
        tests.push(format!("fluster{i}+skip"));
    }

    let result = FlusterMock::new()
        .with_baseline(baseline)
        .with_decoder(decoder)
        .run(tests)
        .unwrap();

    assert_eq!(result.stderr, "");
    assert_eq!(result.status.code(), Some(0));

    assert!(result.stdout.contains("Pass: 10"));
    assert!(result.stdout.contains("Skip: 11"));
    assert!(result.stdout.contains("ExpectedFail: 12"));
    let results = result.results.unwrap();
    assert_eq!(results.result_counts.pass, 10);
    assert_eq!(results.result_counts.skip, 11);
    assert_eq!(results.result_counts.expected_fail, 12);
}

#[test]
fn many_passes() {
    let mut tests = vec![];
    let decoder = "my-decoder-SW";
    for i in 0..1000 {
        tests.push(format!("test{i}+pass"));
    }

    let result = FlusterMock::new().with_decoder(decoder).run(tests).unwrap();
    assert_eq!(result.stderr, "");
    assert_eq!(result.status.code(), Some(0));
    assert!(result.stdout.contains("Pass: 1000"));
    assert_eq!(result.results.unwrap().result_counts.pass, 1000);
}

/// Test detection of a crash after fluster reported a result.
#[test]
fn late_crash() {
    let decoder = "my-decoder-SW";
    let tests = vec!["fluster+late_crash".to_string()];
    let result = FlusterMock::new().with_decoder(decoder).run(tests).unwrap();
    assert!(result.stderr.contains("fluster+late_crash"));
    assert_eq!(result.status.code(), Some(1));
    assert!(result.stdout.contains("Crash: 1"));
    assert_eq!(result.results.unwrap().result_counts.crash, 1);
}

#[test]
fn multiple_decoders() {
    let mut baseline = String::new();
    let mut tests = vec![];
    // All decoders will have the codec CODEC -> tests will be run for each decoder
    let decoders = ["my-decoder-SW", "my-decoder-HW", "my-decoder-TEST"];

    for i in 0..12 {
        // my-decoder-HW doesn't support these
        baseline.push_str(&format!("my-decoder-HW@fluster{i}+fail,Fail\n"));
        tests.push(format!("fluster{i}+fail"));
    }
    for i in 0..10 {
        tests.push(format!("fluster{i}+pass"));
    }
    for i in 0..11 {
        tests.push(format!("fluster{i}+skip"));
    }

    let result = FlusterMock::new()
        .with_baseline(baseline)
        .with_decoders(&decoders)
        .run(tests)
        .unwrap();

    // This will run the given vectors (tests) for each given decoder (3)
    // 10 are marked pass -> 30 passes will be seen
    // 11 are skipped -> 33 skips will be seen
    // 12 are marked failed (but they are supposed to for the my-decoder-HW decoder)
    //  -> 12 expected fails will be seen
    //  -> 24 fails will be seen (The 2 other decoders)

    // Each fails are run twice and printed twice on stderr
    assert_eq!(result.stderr.lines().count(), 48);

    // status code is 1 as some tests failed
    assert_eq!(result.status.code(), Some(1));

    assert!(result.stdout.contains("Pass: 30"));
    assert!(result.stdout.contains("Skip: 33"));
    assert!(result.stdout.contains("ExpectedFail: 12"));
    assert!(result.stdout.contains("Fail: 24"));

    let results = result.results.unwrap();
    assert_eq!(results.result_counts.pass, 30);
    assert_eq!(results.result_counts.skip, 33);
    assert_eq!(results.result_counts.expected_fail, 12);
    assert_eq!(results.result_counts.fail, 24);
}
